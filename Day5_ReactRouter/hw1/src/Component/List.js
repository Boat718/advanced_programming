import React from 'react'

export const List = (props) => {
  return (
    <ul>
                {
                    props.todo.map((e,i) => 
                        <li key={i}>Todo: {e[0]} Detail: {e[1]}</li>
                    )
                }
    </ul>

  )
}
