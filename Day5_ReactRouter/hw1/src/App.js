import { BrowserRouter,Route,Routes } from 'react-router-dom';
import './App.css';
import { Navbar } from './Component/Navbar';
import {Home} from './Component/Home'
import {Create} from './Component/Create'
import {List} from './Component/List'
import { useState } from 'react';

function App() {
  const [todo, SetToDo] = useState([])
  return (
    <div>
       <Navbar />
       <Routes>
          <Route path='/' index element={<Home />} />
          <Route path='/create' element={<Create todo={todo} setTodo={SetToDo} />} />
          <Route path='/list' element={<List todo={todo}/>} />
        </Routes>
  </div>

  );
}

export default App;
