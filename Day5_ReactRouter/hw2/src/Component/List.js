import React,{useState,useEffect} from 'react'

export const List = (props) => {

  const [list, Setlist] = useState([])

  useEffect(()=>{
    async function fetchData (){
      const response = await fetch('https://todo.showkhun.co/lists')
      const data = await response.json()
      return data
    }

    fetchData().then((data)=> Setlist(data.lists))
  },[])


  return (
    <ul>
      {list.map((todo) => <li key={todo.id}>Todo: {todo.todo} Detail: {todo.detail}</li>)}
    </ul>

  )
}
