import { Route,Routes } from 'react-router-dom';
import './App.css';
import { Navbar } from './Component/Navbar';
import {Home} from './Component/Home'
import {Create} from './Component/Create'
import {List} from './Component/List'
import { useState } from 'react';

function App() {
  const [items, setItem] = useState([])
  return (
    <div>
       <Navbar />
       <Routes>
          <Route path='/' index element={<Home />} />
          <Route path='/create' element={<Create items={items} setItem={setItem} />} />
          <Route path='/list' element={<List items={items}/>} />
        </Routes>
  </div>

  );
}

export default App;
