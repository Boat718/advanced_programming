import React from 'react'
import Header from './Header'
import TodoList from './TodoList'
import './App.css'

const App = props => (
  <div className="app">
    <Header />
    <TodoList />
  </div>
)

export default App
