import './App.css';
import React from "react";
import Card from "./Card";

const color = ["red", "blue", "green", "purple", "pink"];



export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // shuffle state
      decks: []
    };
  }

  

  shuffle = ([...arr]) => {
    // shuffle logic
    let m = arr.length;
    while (m) {
      const i = Math.floor(Math.random() * m--);
      [arr[m], arr[i]] = [arr[i], arr[m]];
    }
    return arr;
  };

  handleShuffle = () => {
    // shuffle card
    const colorset = color.concat(color)
    this.setState({decks:this.shuffle(colorset)})
  };

  funccolor = (color) =>{
    alert(color)
  }

  render() {
    console.log(this.state.decks)
    return (
      <div>
        <div className="card-wrap">
          {this.state.decks.map((e,i)=>{
           return <Card background={e} key = {i} />
          })}
        </div>
        <div className="text-center">
          <button onClick={this.handleShuffle} >new deck</button>
        </div>
      </div>
    );
  }
}
