import './App.css';
import React, { useState } from 'react';

function App() {

  const [info, setInfo] = useState([])

  const generate = () =>{
    fetch("https://randomuser.me/api/")
      .then(res=> res.json())
      .then(data =>setInfo(data.results))
  }

  const [data] = info

  return (
    <div className="App">
      <img src={data.picture.large} alt="pic" />
      <p>E-mail: {data.email}</p>
      <p>Gender: {data.gender}</p>
      <p>{data.name.title} {data.name.first} {data.name.last}</p>
      <button onClick={generate}> Generate User</button>
    </div>
  );
}

export default App;
