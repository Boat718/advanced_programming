import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navvbar from './Navbar';
import PictureCard from './PictureCard';
import { CardGroup} from "react-bootstrap";

// //id = id ของ pikka
// imgSrc = url ภาพ
// createBy = ชื่อคนโพสภาพ
// date = วันที่โพส
// likeCount = จำนวน like
// commentCount = จำนวน Comment


function App() {

  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth()+1;
  const year = date.getFullYear();
  const today = day + "/ "+month+" /"+year;

  return (
    <div>
     <Navvbar/>
     <CardGroup>
     <PictureCard 
      id={1} 
      imgSrc = {`https://picsum.photos/id/1/281/160`}
      createBy = {"Name"}
      date = {today}
      likeCount = {99}
      commentCount = {"9999+"}
      />
       <PictureCard 
      id={2} 
      imgSrc = {`https://picsum.photos/id/2/281/160`}
      createBy = {"Name"}
      date = {today}
      likeCount = {99}
      commentCount = {"9999+"}
      />
       <PictureCard 
      id={3} 
      imgSrc = {`https://picsum.photos/id/3/281/160`}
      createBy = {"Name"}
      date = {today}
      likeCount = {99}
      commentCount = {"9999+"}
      />
       <PictureCard 
      id={4} 
      imgSrc = {`https://picsum.photos/id/4/281/160`}
      createBy = {"Name"}
      date = {today}
      likeCount = {99}
      commentCount = {"9999+"}
      />
       <PictureCard 
      id={5} 
      imgSrc = {`https://picsum.photos/id/5/281/160`}
      createBy = {"Name"}
      date = {today}
      likeCount = {99}
      commentCount = {"9999+"}
      />
      </CardGroup>
    </div>
  );
}

export default App;
