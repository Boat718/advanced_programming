import React from "react";
import {Container, Nav, Navbar} from "react-bootstrap";

function Navvbar() {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">
        <img src="https://raw.githubusercontent.com/panotza/pikkanode/master/pikkanode.png" alt = "pic" width="100"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav"  className="me-auto d-flex justify-content-end">
          <Nav>
            <Nav.Link href="#home" >Create pikka</Nav.Link>
            <Nav.Link href="#home" >Sign Up</Nav.Link>
            <Nav.Link href="#home">Sign In</Nav.Link>
            <Nav.Link href="#link">Sign Out</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )

}

export default Navvbar;

