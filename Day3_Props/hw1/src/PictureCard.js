import React from "react";
import { Card } from "react-bootstrap";

function PictureCard({id,imgSrc,createBy,date,likeCount,commentCount}) {
    return(
        <div>
        <Card className="ms-2 mt-2">
        <Card.Img variant="top" src={imgSrc} />
        <Card.Body>
            <Card.Title>Picture title</Card.Title>
            <Card.Text>
            <h4>id: {id}</h4>
            <h4>Create by: {createBy}</h4>
            <p>Comment: {commentCount}</p>
            <p>Like : {likeCount}</p>
            </Card.Text>
        </Card.Body>
        <Card.Footer>
            <small className="text-muted">{date}</small>
        </Card.Footer>
        </Card>
        </div>

    )
};

export default PictureCard;
