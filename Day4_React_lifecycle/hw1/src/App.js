/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import "./App.css";

export default class App extends Component {
  state = {
    lists: [],
    loading: true,
    show: true,
    detail: "ddd",
    todo: "dddd"
  };

  async componentDidMount() {
    try {
      const response = await fetch('https://todo.showkhun.co/lists');
      const {lists} = await response.json();
      // console.log(lists[0])
      this.setState({
        lists:lists
      })
    } catch (err) {
      alert(err.message)
    } finally {
        this.setState({ loading: false })
    }
  }

  
  handleCloseDetail = () => {
    this.setState({
      show: false
    });
  };

  render() {
    const listsArray = this.state.lists;
    console.log(this.state.loading)
    if(this.state.loading){
     return <div>Loading</div>
    }
      return (
        <div className="App">
          <h1>Hello TODOLIST</h1>
          <ul>
            <li>
              <a href="#">todo loop</a>
            </li>
          </ul>
          {
          listsArray.map(e=>{
            return this.state.show ? (
              <Detail
                todo={e.todo}
                detail={e.detail}
                show={this.state.show}
                loading={this.state.loading}
                onClose={this.handleCloseDetail}
              />
            ) : (
              false
            )
          })
          }
        </div>
      );
  }
}

export class Detail extends Component {
  render() {
    return (
      <div
        style={{
          background: "#eee",
          zIndex: 999,
          width: "60%",
          margin: "0 auto",
          textAlign: "left",
          height: 400,
          display: "block",
          padding: 10,
          position: "relative"
        }}
      >
        <button
          style={{
            position: "absolute",
            top: 10,
            left: "auto",
            right: 10
          }}
          onClick={this.props.onClose}
        >
          x
        </button>
        <strong>Detail</strong>

        <p>Todo : {this.props.todo}</p>
        <p>Detail : {this.props.detail}</p>
      </div>
    );
  }
}
