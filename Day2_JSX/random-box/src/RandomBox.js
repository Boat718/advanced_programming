import React from 'react'



function RandomBox(){

    const arrColor = ["red","blue","green","purple","pink"];
    //font: 20-40px

    const refresh = ()=>{
        let indexColor = Math.floor(Math.random() * 5);
        let fontsize= Math.floor(Math.random() * (41 -20) ) + 20;
        box.background = arrColor[indexColor];
        font.fontSize = fontsize+"px";
    }

    const box = {
        
        width:"300px",
        height:"300px",
        margin:"0 auto",
        background:"red",
        color:"white",
        textAlign:"center",
        position: "relative"
    }

    const font = {
        display: "inline-block",
        top:"50%",
        left:"50%",
        position:"absolute",
        transform:" translate(-50%, -50%)",
    }

    refresh();

    return(
        <div style={box}>
            <span style={font}>
             Random Box
            </span>
            
        </div>
    )
}

export default RandomBox;