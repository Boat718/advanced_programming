import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navvbar from './Navbar';
import PictureCard from './PictureCard';
import Footer from './Footer';

function App() {
  return (
    <div>
     <Navvbar/>
     <PictureCard/>
     <Footer/>
    </div>
  );
}

export default App;
