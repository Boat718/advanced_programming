import React from "react";
import { CardGroup, Card } from "react-bootstrap";

function PictureCard() {
  const x = () => {
    let arr = [];
    for (let i = 0; i < 4; i++) {
        let rand = Math.floor(Math.random() * 300);
        let src = `https://picsum.photos/id/${rand}/281/160`;
        const date = new Date();
        const day = date.getDate();
        const month = date.getMonth()+1;
        const year = date.getFullYear();
        const total = day + "/ "+month+" /"+year;
        arr.push(
            <Card className="ms-2 mt-2">
            <Card.Img variant="top" src={src} />
            <Card.Body>
                <Card.Title>Picture title</Card.Title>
                <Card.Text>
                <h4>Like 24</h4>
                <h4>Comment 99+</h4>
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                <small className="text-muted">{total}</small>
            </Card.Footer>
            </Card>
        );
    }
    return(
        <CardGroup>
            {arr}
        </CardGroup>
    )
  };

  return (
    [x(),x(),x()]
    );
}

export default PictureCard;
