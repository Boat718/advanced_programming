import React from "react";
import { Navbar, Container } from "react-bootstrap";

function Footer() {
  return (
    <div className="mt-3">
      <Navbar bg="dark">
        <Container>
            <img
                src="https://raw.githubusercontent.com/panotza/pikkanode/master/pikkanode.png"
              className="mx-auto d-block"
              alt="logo"
              width="350"
            />
        </Container>
      </Navbar>
    </div>
  );
}

export default Footer;
