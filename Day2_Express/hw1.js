const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }))


//ให้ทำการสร้าง File hw1.js โดยให้สร้าง Router ในการ รับข้อมูล เลขจำนวน 4 ตัวเลข โดยตัวเลขทั้ง 4 จะต้องเป็นจำนวนที่ไม่เกิน 1-9 
//ถ้าหากมีตัวเลขใดใน 4 ตัวเป็นเลขที่มีค่าเกินกว่า 1-9 ให้ทำการ Return Response เป็น 403 กลับไป แต่ถ้าตัวเลขทั้ง 4 ตัวเลขนั้น เป็นตัวเลข 1-9 ทั้งหมด 
//ให้นำตัวเลขทั้ง 4 นั้นมาคำนวนว่า สามารถ บวก ลบ คูณ หรือ หาร แล้วได้เลขเป็น 24 หรือไม่ 
//ถ้าหากได้ให้ Return Response สูตรในการคำนวน และ บอกว่า Success แต่ถ้าไม่สามารถทำได้ให้ Return Response บอกว่า Fail


app.post("/post",(req,res)=>{
    const number = String(req.body.number).split("");

    if(number.length == 4){
        const [n1,n2,n3,n4] = number.map((e)=>Number(e));
        const result = do24(n1,n2,n3,n4)
        if(result){
            res.send(result + "Success");
        }
        else{
            res.send("Fail");
        }
    }
    else{
        res.sendStatus(403)
    }
})

app.listen(port,()=>{
    console.log("Server is running")
})



function do24( a,b,c,d ){

    value = ''
    nums = ["",a,b,c,d]
    ops= ["","+","-","*","/"]
    maxsol = 10
    sol = 0

    for (i=1; i<5 && sol<maxsol; i++) {
     for (j=1; j<5 && sol<maxsol; j++) {
      if (j==i)
        continue
      for (k=1; k<5 && sol<maxsol; k++) {
       if (k==i | k==j
    )    continue
       l=1+2+3+4-i-j-k

       for (p=1; p<5 && sol<maxsol; p++) {
        for (q=1; q<5 && sol<maxsol; q++) {
         for (r=1; r<5 && sol<maxsol; r++) {
    
          for (p1o=1; p1o<4 && sol<maxsol; p1o++) {
           for (p1c=p1o+1; p1c<5 && sol<maxsol; p1c++) {
            for (ispecial=1; ispecial<=2; ispecial++) {
    
            if (ispecial==1 && p1o==1 && p1c==2) {
                p2o=3; p2c=4
            } else {
                p2o=0; p2c=0
            }
            expr  = p1o==1 ? '(' : ''
            expr += nums[i] + ops[p] + (p1o==2 ? '(' : '')
            expr += nums[j] + (p1c==2 ? ')' : '')
            expr += ops[q] + (p1o==3 || p2o==3 ? '(' : '')
            expr += nums[k] + (p1c==3 ? ')' : '')
            expr += ops[r] + nums[l] + (p1c==4 || p2c==4 ? ')' : '')
            ev = eval(expr)

                if (ev>23.9999 && ev<24.00001) {
                    if (sol == 0) value = '';
                    expr = expr + ' = ' + Math.round(ev);
                        if (value.indexOf(expr)<0)
                        {
                            value += expr +"\n ";
                        sol++
                    }
                } 
            } 
           } 
          } 
         } 
        } 
       } 
      } 
     } 
    } 
    if (sol == 0)
         return false
    else
        return value;
    }
    