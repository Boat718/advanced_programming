const http = require('http');
const date = new Date();

http.createServer((req,res)=>{
    res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
    res.write('Kittikawin Pawabut  '+date);
    res.end();
}).listen(2337);

